from collections import defaultdict, OrderedDict


class BPE:
    def __init__(self, daten="", anzahl=1, isfile=False):
        self.traindaten = daten
        self.anzahl_op = anzahl
        self.isfile = isfile
        self.operationen = []
        self.zerlegung = 0
        self.vorkommendict = self.zerlegen()

    # erlernte BPE Operationen auf Text anwenden
    def ops_apply(self):
        return 0

    # BPE Zerlegung rueckgaengig machen
    def ops_undo(self):
        return 0

    # zerlegt die Traindaten als Zeile in Paare und zaehlt ihre Vorkommen
    # Rueckgabe als Dictionary, beginnend mit hoechsem Vorkommen
    def zerlegen(self):
        d = defaultdict(int)
        for i in range(0, len(self.traindaten) - 1):
            paar = self.traindaten[i:i + 2]
            if paar.isalpha():
                if self.traindaten[i + 2].isspace():
                    # falls das aktuelle Paar sich ganz am Ende befindet
                    # oder nach dem Paar ein Leerzeichen kommt
                    paar += "</w>"
                d[paar] += 1

        ordict = OrderedDict()
        # nach der Anzahl in absteigender Reihenfolge sortieren
        for key, val in sorted(d.items(), key=lambda item: item[1], reverse=True):
            ordict[key] = val
        return ordict

    # zieht das haeufigste Paar zusammen und gibt dieses zurueck
    # als String
    def zusammenziehen(self):
        return self.vorkommendict.popitem(last=False)[0]

    # generiert neue zu zaehlende Paare mit dem haeufigsten Paar
    # und erstellt daraus ein neues OrderedDict
    def neue_paare(self):
        most = self.zusammenziehen()
        neuepaare = []
        for key, val in self.vorkommendict.items():
            if most[-1] == key[0]:
                # zusammensetzen der beiden "Teile"
                neuepaare.append(most[0:-1] + key)
            elif key[-1] == most[0]:
                # zusammensetzen der beiden "Teile"
                neuepaare.append(key[0:-1] + most)
            else:
                # falls eine Zusammensetzung nicht moeglich
                neuepaare.append(key)

        # Vorkommen jedes der Paare zaehlen
        d = defaultdict(int)
        for paar in neuepaare:
            if paar[-4:] == "</w>":
                # wenn abgetrenntes Wort, muss das Wort inkl. Leerzeichen gesucht werden
                d[paar] = self.traindaten.count(paar[:-4] + " ")
            else:
                d[paar] = self.traindaten.count(paar) - self.traindaten.count(paar + " ")

        ordict = OrderedDict()
        # nach der Anzahl in absteigender Reihenfolge sortieren
        for key, val in sorted(d.items(), key=lambda item: item[1], reverse=True):
            ordict[key] = val

        self.vorkommendict = ordict


if __name__ == "__main__":
    satz = "Especially when one compares them with current online maps . Then it becomes clear how the towns " \
           "and municipalities in the distribution area of Munich 's Merkur newspaper have changed since the 19th " \
           "century . The digital BayernAtlas is a patchwork quilt that one doesn ' t quite recognise for " \
           "what it is at first . "
    bpe_satz = BPE(satz.lower(), 2)
    print(bpe_satz.vorkommendict)
    for _ in range(3):
        bpe_satz.neue_paare()
        print(bpe_satz.vorkommendict)
