# 1. Uebung
# Lennard Heller (366733) Elena Kortmann (395205)
from levenshtein import Levenshtein
from bleu import Bleu
import argparse


class Metrik:
    def __init__(self, hypothese="", referenz="", isfile=False):
        self.hypothese = hypothese
        self.referenz = referenz
        self.N = 4
        self.levenshtein = Levenshtein(hypothese, referenz, isfile)
        self.bleu = Bleu(hypothese, referenz, isfile)
        self.wer = self.levenshtein.distanz / self.bleu.ref_length
        self.per = self.calc_per()

    def calc_per(self):
        zaehler = self.levenshtein.matches - max(0, self.bleu.hypo_length - self.bleu.ref_length)
        nenner = self.bleu.ref_length
        return 1 - zaehler / nenner


if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage="\nBerechne die Metrik zweier Saetze")
    parser.add_argument("-1", "--hypothese", type=str, help="erster zu untersuchender String")
    parser.add_argument("-2", "--referenz", type=str, help="zweiter zu untersuchender String")
    parser.add_argument("-f1", "--file1", help="Dateipfad zur Hypothese")
    parser.add_argument("-f2", "--file2", help="Dateipfad zur Referenz")
    parser.add_argument("-L", help="Berechne Levenshtein-Distanz", action="store_true")
    parser.add_argument("-o", "--ops", help="Gebe Operationen zur Levenshtein-Distanz aus", action="store_true")
    parser.add_argument("-B", "--bleu", help="Berechne BLEU-Metrik", action="store_true")
    parser.add_argument("-W", "--wer", help="Berechne WER", action="store_true")
    parser.add_argument("-P", "--per", help="Berechne PER", action="store_true")
    args = parser.parse_args()

    if args.hypothese and args.referenz:
        metrik_satz = Metrik(args.hypothese, args.referenz, False)

        if args.ops:
            for op in metrik_satz.levenshtein.lev_ops:
                print(op)

        if args.L:
            print("\nLevenshtein-Distanz: {}".format(metrik_satz.levenshtein.distanz))

        if args.wer:
            print("\nWER: {}".format(metrik_satz.wer))

        if args.per:
            print("\nPER: {}".format(metrik_satz.per))

        if args.bleu:
            print("\nBLEU: {}".format(metrik_satz.bleu.bleuval))

    elif args.file1 and args.file2:
        metrik_korpus = Metrik(args.file1, args.file2, True)

        if args.ops:
            for i in range(len(metrik_korpus.levenshtein.lev_ops)):
                print("\nZeile {}".format(i+1))
                for op in metrik_korpus.levenshtein.lev_ops[i]:
                    print(op)

        if args.L:
            print("\nLevenshtein-Distanz: {}".format(metrik_korpus.levenshtein.distanz))

        if args.wer:
            print("\nWER: {}".format(metrik_korpus.wer))

        if args.per:
            print("\nPER: {}".format(metrik_korpus.per))

        if args.bleu:
            print("\nBLEU: {}".format(metrik_korpus.bleu.bleuval))
