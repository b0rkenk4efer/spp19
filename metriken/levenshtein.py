# 1. Uebung
# 2. Aufgabe
# Lennard Heller (366733) Elena Kortmann (395205)


class Levenshtein:
    def __init__(self, hypothese="", referenz="", isfile=False):
        self.hypothese = hypothese
        self.referenz = referenz
        self.matches = 0
        self.matrix = self.levenshtein_matrix() if not isfile else []
        self.distanz = self.matrix[-1][-1] if not isfile else 0
        self.lev_ops = self.calc_ops() if not isfile else []
        if isfile:
            self.process_lev_file()

    def levenshtein_matrix(self):
        # initialisiere Matrix mit Nullen
        # und lege Laenge und Breite fest
        matrix = []
        zeilen = len(self.referenz) + 1
        spalten = len(self.hypothese) + 1

        for _ in range(zeilen):
            matrix.append(spalten * [0])

        # lege die ersten Werte der ersten Spalte fest
        for i in range(spalten):
            matrix[0][i] = i

        # lege die ersten Werte der ersten Zeile fest
        for j in range(zeilen):
            matrix[j][0] = j

        # iteriere ueber die Matrix
        for z in range(1, zeilen):
            for s in range(1, spalten):
                if self.referenz[z-1] == self.hypothese[s-1]:
                    # gleiche Zeichen
                    matrix[z][s] = matrix[z-1][s-1]    # Match

                else:
                    # unterschiedliche Zeichen
                    matrix[z][s] = min(
                        matrix[z-1][s-1] + 1,   # Substitution
                        matrix[z-1][s] + 1,     # Insertion
                        matrix[z][s-1] + 1      # Deletion
                    )

        return matrix

    def calc_ops(self):
        # beginne in der letzten Zelle der Matrix
        zeile = len(self.referenz)
        spalte = len(self.hypothese)
        ops = []

        # solange man sich nicht in der ersten Zelle der Matrix befindet
        while not (zeile == 0 and spalte == 0):
            pos = self.matrix[zeile][spalte]

            # setze default Werte falls der Eintrag nicht existiert
            # oder er nicht die notwendigen Eigenschaften hat
            links = -1
            darueber = -1
            diagonal = -1

            # pruefe und dokumentiere Wert links von der aktuellen Position
            if spalte - 1 >= 0:
                # pruefe ob Wert links um 1 kleiner
                if pos - 1 == self.matrix[zeile][spalte - 1]:
                    links = self.matrix[zeile][spalte - 1]

            # pruefe und dokumentiere Wert oberhalb der aktuellen Position
            if zeile - 1 >= 0:
                # pruefe ob Wert darueber um 1 kleiner
                if pos - 1 == self.matrix[zeile - 1][spalte]:
                    darueber = self.matrix[zeile - 1][spalte]

            # pruefe und dokumentiere Wert oberhalb links der aktuellen Position
            if zeile - 1 >= 0 and spalte - 1 >= 0:
                diagonal = self.matrix[zeile - 1][spalte - 1]

            # Welcher ist der niedrigste zu erreichende Wert
            # von der aktuellen Position aus?
            minimum = min([val for val in [links, darueber, diagonal] if val > -1])

            if minimum == pos - 1:
                if minimum == darueber:
                    # Einfuegung
                    ops.insert(0, "Einfuegung an Position {}".format(max(zeile, spalte)))
                    zeile -= 1
                elif minimum == diagonal:
                    # Ersetzung
                    ops.insert(0, "Ersetzung an Position {}".format(max(zeile, spalte)))
                    zeile -= 1
                    spalte -= 1
                elif minimum == links:
                    # Loeschung
                    ops.insert(0, "Loeschen an Position {}".format(max(zeile, spalte)))
                    spalte -= 1
            elif minimum == pos and minimum == diagonal:
                # Match
                self.matches += 1
                zeile -= 1
                spalte -= 1

        return ops

    def process_lev_file(self):
        ops = []
        with open(self.hypothese, "r", encoding="utf-8") as file1, open(self.referenz, "r", encoding="utf-8") as file2:
            for line_file1, line_file2 in zip(file1, file2):
                lev_line = Levenshtein(line_file1.strip(), line_file2.strip(), False)
                self.matches += lev_line.matches
                self.distanz += lev_line.distanz
                ops.append(lev_line.lev_ops)
        self.lev_ops = ops
