# 1. Uebung
# 3. Aufgabe
# Lennard Heller (366733) Elena Kortmann (395205)
import math


# generiere ein n-gram fuer einen Satz
def gen_ngram(n, satz):
    ngrams = []
    satz_splitted = satz.strip().split()
    start = 0
    while start <= len(satz_splitted) - n:
        ngrams.append(satz_splitted[start:start + n])
        start += 1
    return ngrams


# berechne die Anzahl der n-grams der Hypothese,
# die ebenfalls in den n-grams der Referenz enthalten sind
# (jeder n-gram Block wird in der Hypothese nur einmal betrachtet)
def calc_ngram_matches(ngram_hypothese, ngram_referenz):
    gesehen = []
    matches = 0                     # Vorkommen der jeweiligen n-grams in der Referenz
    count_ngram_total = 0           # Vorkommen der jeweiligen n-grams in der Hypothese
    for block in ngram_hypothese:
        if block not in gesehen:
            count_ngram_total += ngram_hypothese.count(block)
            matches += ngram_referenz.count(block)
        gesehen.append(block)
    return count_ngram_total, matches


class Bleu:
    def __init__(self, hypothese="", referenz="", isfile=False):
        self.hypothese = hypothese
        self.referenz = referenz
        self.hypo_length = len(self.hypothese) if not isfile else 0
        self.ref_length = len(self.referenz) if not isfile else 0
        if isfile:
            self.calc_hypo_ref_length()
        self.N = 4
        self.brev_penalty = self.set_brev_penalty()
        self.bleuval = self.calc_bleuval(isfile)

    # Falls Hypothese und Referenz eine Datei ist
    def calc_hypo_ref_length(self):
        with open(self.hypothese, "r", encoding="utf-8") as file1, open(self.hypothese, "r", encoding="utf-8") as file2:
            for line_file1, line_file2 in zip(file1, file2):
                self.hypo_length += len(line_file1.strip())
                self.ref_length += len(line_file2.strip())

    def set_brev_penalty(self):
        if self.hypo_length > self.ref_length:
            return 1
        else:
            return math.exp(1 - (self.ref_length / self.hypo_length))

    def calc_bleuval(self, isfile):
        if not isfile:

            # Ausdruck in der Klammer der BLEU Berechnung
            ausdruck = 0.0

            # fuer alle n-grams von 1 bis N
            for i in range(1, self.N + 1):
                ngram_hypo = gen_ngram(i, self.hypothese)
                ngram_ref = gen_ngram(i, self.referenz)

                # Anzahl der Vorkommen jedes n-grams in der Hypothese
                # Anzahl der Vorkommen jedes n-grams in der Referenz (Match)
                ngram_hypo_count, matches_count = calc_ngram_matches(ngram_hypo, ngram_ref)
                if not ngram_hypo_count == 0:
                    mod_ngram_prec = matches_count / ngram_hypo_count

                    if mod_ngram_prec == 0.0:
                        # weil ln(1) = 0 und ln gegen 0 geht gegen unendlich
                        mod_ngram_prec = 1.0
                    ausdruck += 1 / self.N * math.log(mod_ngram_prec)

            return self.brev_penalty * math.exp(ausdruck)

        else:
            ngram_matches = self.N * [0]  # n-grams aus Hypothese, die auch in Referenz enthalten sind
            ngram_hypo_total = self.N * [0]  # Anzahl der n-grams, die sich aus Hypothese bilden lassen
            mod_ngram_arr = self.N * [0]  # Werte fuer modified n-gram Precision von 1 bis n (gesamter Korpus)

            with open(self.hypothese, "r", encoding="utf-8") as file1, \
                    open(self.referenz, "r", encoding="utf-8") as file2:
                for line_file1, line_file2 in zip(file1, file2):
                    # fuer alle n-grams von 1 bis N
                    for i in range(1, self.N + 1):
                        ngram_hypo = gen_ngram(i, line_file1)
                        ngram_ref = gen_ngram(i, line_file2)

                        ngram_hypo_count, matches_count = calc_ngram_matches(ngram_hypo, ngram_ref)
                        ngram_matches[i - 1] += matches_count
                        ngram_hypo_total[i - 1] += ngram_hypo_count

            # lege Liste fuer modifizierte n-gram Praezision 1 bis N an
            for j in range(self.N):
                mod_ngram_arr[j] += ngram_matches[j] / ngram_hypo_total[j]

            # Ausdruck in der Klammer der BLEU-Berechnung
            ausdruck = 0.0
            for k in range(self.N):
                ausdruck += 1 / self.N * math.log(mod_ngram_arr[k])

            return self.brev_penalty * math.exp(ausdruck)
